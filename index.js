console.log(`s25 discussion`)

// JSON Objects
/*
	Syntax:
		{
			"propertyA" : "valueA",
			"propertyB" : "valueB"
		}

	- JSON objects are usually produced, not hard-coded
*/

// examples:
/*{
	"city" : "Quezon City",
	"province" : "Metro Manila",
	"country" : "Philippines"
}*/

// JSON Arrays
/*"cities" : [
	{
		"city" : "Quezon City",
		"province" : "Metro Manila",
		"country" : "Philippines"
	},
	{
		"city" : "Makati City",
		"province" : "Metro Manila",
		"country" : "Philippines"
	},
	{
		"city" : "Manila City",
		"province" : "Metro Manila",
		"country" : "Philippines"
	}
]*/

// JSON Methods

let batchesArr = [
	{
		batchName: 'Batch 182'
	},
	{
		batchName: 'Batch 183'
	}
]
// "stringify" - will convert Javascript objects into a string
console.log('Result from stringify method:');
console.log(JSON.stringify(batchesArr));

let data = JSON.stringify({
	name: 'Eren',
	age: 18,
	address: {
		city: 'Manila',
		country: 'Philippine'
	}
});
console.log(data);

// Stringify method with variables

let firstName = prompt('What is your first name?');
let lastName = prompt('What is your last name?');
let age = prompt('What is your age?');
let address = {
	city: prompt('Which city do you live in?'),
	country: prompt('Which country does your city belongs to?')
};

let otherData = JSON.stringify({
	firstName: firstName,
	lastName: lastName,
	age: age,
	address: address
});
console.log(otherData);

// Converting stringified JSON into JS Objects
	// JSON.parse()

let batchesJSON = `[
	{
		"batchName" : "Batch 125"
	},
	{
		"batchName" : "Batch 126"
	}
]`
console.log('Result from JSON.parse method:');
console.log(JSON.parse(batchesJSON));

let stringifiedObject = `{
	"name" : "Mikasa",
	"age" : 16,
	"address" : {
		"city" : "Tokyo",
		"country" : "Japan"
	}
}`

console.log('Result from JSON.parse method:');
console.log(JSON.parse(stringifiedObject));
